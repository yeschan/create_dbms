//
//  main.c
//  Lab11
//
//  Created by eungchan Kang on 2021/05/13.
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define Time 1

typedef struct _DisjointSet{
    int size_maze;
    int *ptr_arr;
}DisjointSets;

void init(DisjointSets *Sets, DisjointSets *maze_print, int num)
{
    Sets->size_maze = (num * num);  // Set's size(blank size) : num X num
    Sets->ptr_arr = (int*)malloc(sizeof(int) * (Sets->size_maze+1));
    for(int i = 1; i <= Sets->size_maze; i++)
    {
        Sets->ptr_arr[i] = 0;
    }
    maze_print->size_maze = (Sets->size_maze * 2);  // wall size : blank size * 2
    maze_print->ptr_arr = (int*)malloc(sizeof(int) * (maze_print->size_maze+1));
    for(int i = 1; i <= maze_print->size_maze; i++)
    {
        maze_print->ptr_arr[i] = -1;  //(wall -> -1 : yes, 0 : broken)
    }
}

int find(DisjointSets *Sets, int i)
{
    while (Sets->ptr_arr[i] > 0)  // until meet root, the root is negative or zero value
    {
        i = Sets->ptr_arr[i];
    }
    return i;
}

void union_(DisjointSets *Sets, int i, int j)
{
    int r1 = find(Sets, i);  // find root for i
    int r2 = find(Sets, j);  // find root for j
    
    // disk space를 줄이기 위해 height는 음수로 표시
    if(Sets->ptr_arr[r2] < Sets->ptr_arr[r1])  // height of r2 > height of r1
        Sets->ptr_arr[r1] = r2;
    else
    {
        if(Sets->ptr_arr[r1] == Sets->ptr_arr[r2]) // same height, height++;
            Sets->ptr_arr[r1]--;  // more children, more height
        Sets->ptr_arr[r2] = r1;
    }
    
}

void createMaze(DisjointSets *Sets, DisjointSets *maze_print, int num)
{
    printf("Wait a minute...\n");
    printf("Building a Maze [0%% ");
    int wall, cell1, cell2;
    int size = maze_print->size_maze+1;
    int floor = size - num;
    int loop_condition;
    int progress_bar = 1;
    
    while(find(Sets, 1) != find(Sets, Sets->size_maze))
    {
        do
        {
            loop_condition = 1;  // break, if loop_condition is zero
            srand((unsigned int)time(NULL));  // define the current time for seed
            wall = rand() % size;
            
            if(wall == 0)  // 0 means 1 for an index of the wall.
                wall = 1;
            if(maze_print->ptr_arr[wall] == 0) // maze_print->ptr_arr[wall] != 0 : 벽이 이미 뚫렸는지 아닌지!
                continue;
            else if(wall >= floor) // wall >= floor : 바닥을 뚫지 않기 위해
                continue;
            else if(wall <= Sets->size_maze && wall % num == 0) // wall < Sets->size_maze && wall % num == 0 : 외벽을 뚫지 않기 위해
                continue;
            else
                loop_condition = 0;
        }while(loop_condition);
        
        // wall : 1 ~ sizeof(sets)
        // roof : Set_size ~ maze_size
        if (wall <= Sets->size_maze) // wall
            {
                cell1 = wall;
                cell2 = wall + 1;
            }
        else  // roof
        {
            cell1 = wall - Sets->size_maze;
            cell2 = cell1 + num;
        }

        if(find(Sets, cell1) != find(Sets, cell2)) // if different root, union two sets
        {
            union_(Sets, cell1, cell2);
        }
        maze_print->ptr_arr[wall] = 0;  // break the wall
        if(progress_bar % num == 0 && (100/Sets->size_maze) * progress_bar < 100)
            printf("%d%% ",(100/Sets->size_maze) * progress_bar);
        progress_bar ++;
    }
    while((100/Sets->size_maze) * progress_bar < 100)
        {
            if(progress_bar % num == 0)
                printf("%d%% ",(100/Sets->size_maze) * progress_bar);
            progress_bar++;
        }
    printf("100%%]\n");
}

void printMaze(DisjointSets *Sets, DisjointSets* walls, int num)
{
    // print the top roof
    for(int i = 1; i <= num; i++)
    {
        printf("---");
    }
    printf("\n ");
    // wall : 1 ~ set size
    // roof : Set size ~ maze_size
    int i = 1, size = Sets->size_maze + 1;  // i is for only wall, size is for printing only roof
    for (; i <= Sets->size_maze; i++)
    {
        if(walls->ptr_arr[i] < 0){ // if lower than zero, There is a wall
            if(i == Sets->size_maze)  // To break the last wall
                printf("   ");
            else
            {
                printf("  ⎢");
                sleep(Time);
            }
            if((i % num) == 0) // need new line and print roof
            {
                int j = 1;
                printf("\n");  // new line
                for(;j <= num; j++)
                {
                    if(walls->ptr_arr[size] < 0) // check the roof status(break or ?)
                        printf("---");  // not break
                    else
                        printf("   ");  // if broken
                    size++;
                    sleep(Time);
                }
                if(i < Sets->size_maze)
                    printf("\n⎢");   // rightmost wall
                else
                    printf("\n");
                sleep(Time);
            }
        }
        else  // no wall, broken
        {
            if (i % num == 0)
                printf("   \n");
            else
                printf("   ");
            sleep(Time);
        }
    }
    printf("\n");
}

// destroy structures
void freeMaze(DisjointSets *Sets, DisjointSets *maze_print)
{
    free(Sets);
    free(maze_print);
}


int main(int argc, const char * argv[]) {
    // insert code here...
    
    int num;
    //FILE * maze_size = fopen(argv[1], "r");
    //fscanf(maze_size, "%d", &num);
    printf("Set the size of Maze Matrix : ");
    scanf("%d",&num);
    
    DisjointSets *Sets, *maze_print;
    Sets = (DisjointSets*)malloc(sizeof(DisjointSets));
    maze_print = (DisjointSets*)malloc(sizeof(DisjointSets));
    
    init(Sets, maze_print, num);
    printf("\n");
    createMaze(Sets, maze_print, num);
    printf("\n");
    printMaze(Sets, maze_print, num);
    
    freeMaze(Sets, maze_print);
    return 0;
}
