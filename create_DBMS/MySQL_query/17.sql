Select Count(Distinct P.type) AS 'Number of Type'
From Trainer AS T, CatchedPokemon AS C, Pokemon AS P
Where T.id = C.owner_id And C.pid = P.id And T.hometown = 'Sangnok city';