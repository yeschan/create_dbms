Select E1 AS '1단계 진화', E2 AS '2단계 진화', E3 AS '3단계 진화', Name
from Pokemon,
(Select P.ID-1, P.id, P.id+1 From Pokemon P
Where P.ID in
(Select P.id From Pokemon P, Evolution E
Where P.ID = Before_id And Before_id in
(Select After_id From Evolution Where After_id > Before_id)))
AS Sub(E1, E2, E3) Where ID in (E1, E2, E3) Order By ID;