Select Type, Count(*) AS 'Number of Pokemon'
From Pokemon
Group by Type
Order By Count(*), Type;