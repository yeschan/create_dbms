Select Sum(Stype)
From Pokemon AS P,
( Select P.id, Count(*)
From Pokemon P
Where Type in ('Water', 'Electric','Psythic')
Group by P.id) AS Sub(Sid,Stype)
Where Sid = P.id;