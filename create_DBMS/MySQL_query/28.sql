Select T.Name, AVG(level)
from Trainer AS T, CatchedPokemon AS C, Pokemon AS P
Where T.id = C.owner_id And C.pid = P.id
And P.type in ('Electric','normal')
Group by Name Order By AVG(level);
