Select T.Name, SUM(Level) AS 'Total Number'
From Trainer T, CatchedPokemon C
Where T.ID = C.Owner_id
Group By Owner_id
Order By Sum(Level) DESC, Name ASC LIMIT 1;
