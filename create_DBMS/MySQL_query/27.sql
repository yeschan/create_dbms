Select Name, Max(level)
From CatchedPokemon, Trainer,
(Select T.id From Trainer AS T, CatchedPokemon As C
Where T.ID = C.ID And T.ID in
( Select Owner_id From CatchedPokemon
Group By Owner_id Having Count(*) >= 4)) AS SubID(Sid)
Where Sid = Trainer.ID And Owner_id = Trainer.ID
Group by Name Order By Name;

