Select T.Name
From CatchedPokemon C, Trainer T
Where T.ID = C.Owner_id Group BY Owner_id
Having Count(Pid) - Count(Distinct Pid) >= 1
Order By T.Name;