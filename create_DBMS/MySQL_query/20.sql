Select Name, Count(*) AS 'Number of CatchedPokemon'
From Trainer AS T, CatchedPokemon AS C
Where T.hometown = 'Sangnok city' And Owner_id = T.id
Group By Owner_id
Order By Count(*);