Select Owner_id, Maxcounter
From (Select Max(counting) 
from (Select Count(*)
from CatchedPokemon
Group by Owner_id) AS Sub(Counting))
AS Counter(Counting),
(Select owner_id, Count(*)
From CatchedPokemon Group by Owner_id)
AS Owner(Owner_id, MaxCounter)
where MaxCounter = Counting Order By Owner_id;