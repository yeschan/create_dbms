Select Name From Pokemon,
(Select Type, Rank()
OVER(Order By count(*) DESC) PokemonRank
From Pokemon Group BY Type)
AS Subtable(subType, typeRank)
Where Type = SubType And TypeRank in (1,2);
