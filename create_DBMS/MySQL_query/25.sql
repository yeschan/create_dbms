Select Distinct P.Name
from  Pokemon AS P, Trainer AS T, CatchedPokemon AS C
Where T.id = C.owner_ID And P.id = C.Pid And Owner_id in 
(Select T.ID From Trainer Where T.hometown = 'Sangnok city'
UNION
Select T.ID From Trainer Where T.hometown = 'Brown city')
Order By P.Name;
