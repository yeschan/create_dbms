select T.Name
From Trainer AS T, CatchedPokemon AS C
Where T.id = C.owner_id
Group By T.Name
Having Count(*) >= 3
Order By Count(*) DESC;