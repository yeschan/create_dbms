Select Distinct T.Name
From Evolution AS E, Trainer AS T, CatchedPokemon AS C
Where T.id = C.Owner_id
And After_id = C.Pid
And AFter_id Not in (Select Before_id from Evolution);