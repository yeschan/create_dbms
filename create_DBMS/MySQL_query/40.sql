Select SubCity, C.NickName
From CatchedPokemon AS C,
(Select City.Name, Max(level)
From City, Trainer, catchedPokemon As C 
Where City.Name = Trainer.hometown
And C.owner_id = Trainer.ID
Group By city.name) AS Sub(SubCity, SubLevel)
Where C.Level in (subLevel) Order By SubCity;
