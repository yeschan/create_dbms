Select City.Name, AVG(Level)
From city, Trainer AS T, CatchedPokemon As C
Where City.Name = T.Hometown And T.ID = C.Owner_id
Group By City.Name
Order By AVG(Level);

