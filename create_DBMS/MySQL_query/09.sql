Select Name, AVG(level)
From CatchedPokemon AS C, Trainer AS T, Gym AS G
Where C.Owner_id = T.id And T.id = G.Leader_id
Group by Owner_id
Order By Name;