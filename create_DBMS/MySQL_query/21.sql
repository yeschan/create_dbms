 Select T.Name, Count(*) AS 'Number of CatchedPokemon'
 From Trainer AS T, CatchedPokemon AS C, Gym AS G
 Where T.id = C.owner_id And T.id = G.leader_id
 Group By Owner_id
 Order By Count(*);