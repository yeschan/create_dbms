//
//  main.cpp
//  linked_list
//
//  Created by eungchan Kang on 2021/03/23.

// insert, delete, find, find previous, print 기능을 갖춘 stack을 linked list로 구현

#include <iostream>
#include <string>

using namespace std;
typedef class Node * PtrToNode;
typedef PtrToNode Position;
typedef PtrToNode List;
typedef int ElementType;

class Node{
public:
    ElementType element;
    Position next;
    Node(){}
    List MakeEmpty();
    int IsEmpty(List L);
    int IsLast(Position P, List L);
    Position Find(ElementType X, List L);
    Position FindPrevious(ElementType X, List L);
    void Delete(ElementType X, List L);
    void Insert(ElementType X, List L, Position P);
    void DeleteList(List L);
    void printList(List L);
};

List MakeEmpty(){
    List L = new Node();
    L->element = 0;
    L->next = NULL;
    return L;
}

bool IsEmpty(List L){
    return L->next == NULL;
}

bool IsLast(Position P, List L){
    return P->next == NULL;
}

Position Find(ElementType X, List L){
    Position P = L->next;
    while(P != NULL && P->element != X){
        P = P->next;
    }
    return P;
}

Position FindPrevious(ElementType X, List L){
    Position P = L;
    while(P->next->element != X && P->next != NULL){
        P = P->next;
    }
    return P;
}

void Delete(ElementType X, List L){
    Position P = FindPrevious(X, L);
    if (!IsLast(P, L)){
        Position deleted_node = P->next;
        P->next = deleted_node->next;
        free(deleted_node);
    }
    else{
        cout<<"No element("<<X<<")"<<"to delete"<<endl;
    }
}

void Insert(ElementType X, List L, Position P){
    Position inserted_node = new Node();
    if (inserted_node == NULL)
    {
        perror("Out of space!\n");
        EXIT_FAILURE;
    }
    else if(P){
        inserted_node->element = X;
        inserted_node->next = P->next;
        P->next = inserted_node;
    }
    else{
        inserted_node->element = X;
        inserted_node->next = L->next;
        L->next = inserted_node;
    }
}

void DeleteList(List L){
    Position P = L->next;
    Position Tmp;
    L->next = NULL;
    while(P != NULL)
    {
        Tmp = P->next;
        free(P);
        P = Tmp;
    }
    free(L);
}

void printList(List L){
    Position print_node;
    if(!IsEmpty(L))
    {
        print_node = L->next;
        while(print_node != NULL)
        {
            cout<<print_node->element<<" ";
            print_node = print_node->next;
            
        }
    }
    else
        printf("List is Empty\n");
}


int main(int argc, const char * argv[]) {
    // insert code here...
    
    List chan = MakeEmpty();
    int X;
    for(int i = 10, j = 0; i < 20; i++){
        X = i * 10;
        Insert(X, chan, Find(j,chan));
        j = X;
    }
    cout<<"after insertion : ";
    printList(chan);
    cout<<endl;
    for(int i = 10; i < 19; i++){
        X = i * 10;
        Delete(X, chan);
    }
    cout<<"after deletion : ";
    printList(chan);
    cout<<endl;
    DeleteList(chan);
    return 0;
}



/*
using namespace std;
static int cnum;
class Course{
    int ID;
    char Instructor[10];
    int RoomNr;
public:
    Course(){}
    friend class Student;
    Course(int id, char* instructor, int room);
    void show_course();
};

class Student{
private:
    char Name[10];
    int Number;
    Course courses[10];
public:
    Student(){}
    Student(char * name, Course *course, int num);
    void set_info(char * name, Course *course, int num);
    void get_info(Student S);
    void show_info(int i){
        cout<<this->Name<<endl;
        cout<<this->Number<<endl;
        this->courses[i].show_course();
    }
};

Student::Student(char * name, Course *course, int num){
    strcpy(this->Name, name);
    this->courses[cnum] = *course;
    this->Number = num;
}
void Student::set_info(char * name, Course *course, int num){
    strcpy(this->Name, name);
    this->courses[cnum] = *course;
    this->Number = num;
}

void Student::get_info(Student S){
    cout<<S.Name<<endl;
    cout<<S.Number<<endl;
    //cout<<S.courses<<endl;
}

Course::Course(int id, char * instructor, int room){
    this->ID = id;
    strcpy(this->Instructor, instructor);
    this->RoomNr = room;
}

void Course::show_course(){
    cout<<this->ID<<endl;
    cout<<this->Instructor<<endl;
    cout<<this->RoomNr<<endl;
}

void gen_random(char *s, int len){
    static const char alphabet[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz";
    
    for(int i = 0; i < len; i++){
        s[i] = alphabet[rand() % (sizeof(alphabet)-1)];
    }
    s[len] = 0;
}
int main(int argc, const char * argv[]) {
    // insert code here...
    cnum = 0;
    char Name[10];
    char Instructor[10];
    int course_ID, roomNUM, SID;
    for (int i =0; i < 10; i++){
        gen_random(Name, sizeof(Name)-1);
        gen_random(Instructor, sizeof(Instructor)-1);
        course_ID = rand() % 1000;
        roomNUM = rand()% 100;
        SID = rand() % 100000;
        Course *C = new Course(course_ID, Instructor,roomNUM);
        Student *S = new Student(Name,C, SID);
        S->show_info(cnum);
        cnum++;
        cout<<endl;
    }
    
    
    return 0;
}


*/
