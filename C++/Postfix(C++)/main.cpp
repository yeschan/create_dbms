//
//  main.cpp
//  Postfix
//
//  Created by eungchan Kang on 2021/03/25.
//

// ./main input.txt 로 실행
// input.txt에는 4736%+*42/-9+23*-# 와 같은 Postfix 수식이 있다.
// 해당 수식을 입력 받아 Postfix 계산순서로 계산하여 출력하는 코드

#include <iostream>
#define MaxSize 101
using namespace std;
char operators[] = {'+','-','*','/','%'};
typedef class Node * NodePtr;
typedef NodePtr Stack;

typedef int ElementType;
class Node{
private:
    ElementType key;
    NodePtr next;
public:
    Node(){}
    friend Stack CreateStack();
    friend bool IsEmpty(Stack S);
    friend bool IsFull(Stack S);
    friend ElementType Top(Stack S);
    friend void Push(Stack S, int X);
    friend ElementType Pop(Stack S);
    void Postfix(Stack S, char  input_str);
    friend void DeleteStack(Stack S);
};

Stack CreateStack(){
    Stack S = new Node();
    if(S == NULL)
    {
        perror("Out of Space\n");
        EXIT_FAILURE;
    }
    S->key = 0;
    S->next = NULL;
    
    return S;
}

bool IsEmpty(Stack S){
    return S->next == NULL;
}

ElementType Top(Stack S){
    if(!IsEmpty(S))
        return S->next->key;
    else{
        perror("Stack is Empty\n");
        return 0;
    }
}

void Push(Stack S, int X){
    Stack pushed_node = new Node();
    pushed_node->key = X;
    pushed_node->next = S->next;
    S->next = pushed_node;
}

ElementType Pop(Stack S){
    int picked_element = 0;
    if(!IsEmpty(S)){
        Stack picked_node = S->next;
        S->next = picked_node->next;
        picked_element = picked_node->key;
        free(picked_node);
        }
    else
        perror("Stack is empty\n");
    return picked_element;
}

void DeleteStack(Stack S){
    Stack Del = S->next;
    Stack temp;
    while(Del != NULL){
        temp = Del->next;
        free(Del);
        Del = temp;
    }
    free(S);
}
bool check_operator(char input_str){
    for (int i = 0; i < strlen(operators); i++)
    {
        if(input_str == operators[i])
            return true;
    }
    return false;
}

ElementType Perform_op(int a, int b, char op){
    switch(op){
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a/b;
        case '%':
            return a%b;
        default:
            break;
    }
    return 0;
}

ElementType Postfix(Stack S, char input_str){
        if(input_str > '0' && input_str <= '9'){
            ElementType X = input_str - '0';
            Push(S, X);
            return Top(S);
        }
        else if(check_operator(input_str) == true)
        {
            ElementType a = Pop(S);
            ElementType b = Pop(S);
            ElementType X = Perform_op(b, a, input_str);
            Push(S,X);
            return Top(S);
        }
        else
        {
            perror("Invalid input\n");
            return 0;
        }
}

int main(int argc, const char * argv[]) {
    // insert code here...
    FILE * file = fopen(argv[1], "r");
    
    Stack stack;
    char input_str[MaxSize];
    int result = 0;
    
    fgets(input_str, 101, file);
    stack = CreateStack();
    
    cout<<"Top numbers : ";
    for (int i = 0; i < strlen(input_str) && input_str[i] != '#'; i++){
        result = Postfix(stack, input_str[i]);
        cout<<result<<' ';
    }
    cout<<endl<<"valuation result : "<<result<<endl;
    fclose(file);
    DeleteStack(stack);
    return 0;
}
